'use strict';

const bodyParser = require('body-parser');
const express = require('express');
const app = express();

// allow cors
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())



// mongo config
const dbConfig = require('./config/database.config');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

// connect to mongo
mongoose.connect(dbConfig.uri)
.then(() => {
	console.log("connected to db");
}).catch(err => {
	console.log("cannot connect");
	process.exit;
});

app.get('/', (req, res) => {
    res.json({"message": "Welcome to EasyNotes application. Take notes quickly. Organize and keep track of all your notes."});
});

// requires the notes route
require('./app/routes/note.routes.js')(app);

// requires the places route
require('./app/routes/place.routes.js')(app);

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});

