const Note = require('../models/note.model.js');

// create and save a new note
exports.create = (req, res) => {

  // validate the request
  if(!req.body.content){
    return res.status(400).send({
      message: "Note content cannot be empty"
    });
  }

  // create a note
  const note = new Note({
    title: req.body.title || 'Untitled Note',
    content: req.body.content
  });

  // save notes in the database
  note.save()
    .then(data => {
      res.send(data);
    }).catch(err => {
      res.status(500).send({
        message: err.message || "some error occurred while creating the Notes."
      });
    });

};

// retrieve and return all notes from the database
exports.findAll = (req, res) => {

  Note.find()
      .then(notes => {
        res.send(notes);
      }).catch(err => {
        res.status(500).send({
            message: err.message || "some error occurred while retrieving the Notes."
        });
      });
};

// find a single note with a noteId
exports.findOne = (req, res) => {

  Note.findById(req.params.noteID)
    .then(note => {
      if(!note) {
        return res.status(404).send({
					message: `Note note found with id ${req.params.noteId}`
        });
      }
      res.send(note);
    }).catch(error => {
      if(err.kind === 'ObjectId') {
        return res.status(404).send({
					message: `Note note found with id ${req.params.noteId}`
        });
      }
      return res.status(500).send({
				message: `Error retrieving note with id ${req.params.noteId}`
      });
  });

};

// update a note identified by the noteId in the request
exports.update = (req, res) => {

  // validate request
	if(!req.body.content){
		return res.status(400).send({
			message: "Note content cannot be empty"
		});
	}

	// find note and update it with the request body
  Note.findByIdAndUpdate(req.params.noteId, {
    title: req.body.title || 'Untitled Note',
    content: req.body.content
  }, {new: true})
    .then(note => {
      if(!note) {
        return res.status(404).send({
          message: `Note not found with id ${req.params.noteId}`
        });
      }
      res.send(note);
    }).catch(err => {
      if(err.kind === "ObjectId") {
        return res.status(404).send({
					message: `Note not found with id ${req.params.noteId}`
        });
      }
      return res.status(500).send({
				message: `Note not found with id ${req.params.noteId}`
      });
  });

};

// delete a note with teh specified noteId in the request
exports.delete = (req, res) => {

  Note.findByIdAndRemove(req.params.noteId)
    .then(note => {
      if(!note) {
        return res.status(404).send({
					message: `Note not found with id ${req.params.noteId}`
        });
      }
      res.send({
				message: 'Note deleted successfully!'
      });
    }).catch(err => {
			console.log(err);
    	if(err.kind === 'ObjectId' || err.name === 'NotFound') {
    	  return res.status(404).send({
					message: `Note not found with id ${req.params.noteId}`
        });
      }
      return res.status(500).send({
				message: `Could not delete note with id ${req.params.noteId}`
      });
  });

};










