const Place = require('../models/place.model.js');

// create and save a new note
exports.create = (req, res) => {

  // validate the request
  if(!req.body.pic){
    return res.status(400).send({
      message: "Pics cannot be empty."
    });
  }

  // create a note
  const place = new Place({
    name: req.body.name || 'Unknown Place',
    pic: req.body.pic,
		text: req.body.text || 'Unknown Place'
  });

  // save notes in the database
  place.save()
    .then(data => {
      res.send(data);
    }).catch(err => {
      res.status(500).send({
        message: err.message || "some error occurred while creating the places."
      });
    });

};

// retrieve and return all notes from the database
exports.findAll = (req, res) => {

  Place.find()
      .then(notes => {
        res.send(notes);
      }).catch(err => {
        res.status(500).send({
            message: err.message || "some error occurred while retrieving the places."
        });
      });

};

// find a single note with a placeId
exports.findOne = (req, res) => {

  Place.findById(req.params.placeId)
    .then(place => {
      if(!place) {
        return res.status(404).send({
					message: `Place found with id ${req.params.placeId}`
        });
      }
      res.send(place);
    }).catch(error => {
      if(err.kind === 'ObjectId') {
        return res.status(404).send({
					message: `Place found with id ${req.params.placeId}`
        });
      }
      return res.status(500).send({
				message: `Error retrieving place with id ${req.params.placeId}`
      });
  });

};

// update a note identified by the placeId in the request
exports.update = (req, res) => {

  // validate request
	if(!req.body.pic){
		return res.status(400).send({
			message: "Place picture cannot be empty"
		});
	}

	// find note and update it with the request body
  Place.findByIdAndUpdate(req.params.placeId, {
    name: req.body.name || 'Unknown Place',
    pic: req.body.pic,
		text: req.body.text || 'Unknown Place'
  }, {new: true})
    .then(note => {
      if(!note) {
        return res.status(404).send({
          message: `Place not found with id ${req.params.placeId}`
        });
      }
      res.send(note);
    }).catch(err => {
      if(err.kind === "ObjectId") {
        return res.status(404).send({
					message: `Place not found with id ${req.params.placeId}`
        });
      }
      return res.status(500).send({
				message: `Place not found with id ${req.params.placeId}`
      });
  });

};

// delete a note with teh specified placeId in the request
exports.delete = (req, res) => {

  Place.findByIdAndRemove(req.params.placeId)
    .then(place => {
      if(!place) {
        return res.status(404).send({
					message: `Place not found with id ${req.params.placeId}`
        });
      }
      res.send({
				message: 'Place deleted successfully!'
      });
    }).catch(err => {
			console.log(err);
    	if(err.kind === 'ObjectId' || err.name === 'NotFound') {
    	  return res.status(404).send({
					message: `Place not found with id ${req.params.placeId}`
        });
      }
      return res.status(500).send({
				message: `Could not delete place with id ${req.params.placeId}`
      });
  });

};










