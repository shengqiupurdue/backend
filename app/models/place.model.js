const mongoose = require('mongoose');

const PlaceSchema = mongoose.Schema({
	name: String,
	pic: String,
  text: String
}, {
	timestamps: true
});

module.exports = mongoose.model('Place', PlaceSchema);
