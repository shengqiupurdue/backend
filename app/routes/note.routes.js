module.exports = (app) => {
	const notes = require('../controllers/note.controller.js');

	// create a new Note
	app.post('/notes', notes.create);

	// retrieve all notes
	app.get('/notes', notes.findAll);
	
	// retrieve one note
	app.get('/notes/:noteId', notes.findOne);

	// update a note with a note id
	app.put('/notes/:noteId', notes.update);

	// delete using a note
	app.delete('/notes/:noteId', notes.delete);
};
