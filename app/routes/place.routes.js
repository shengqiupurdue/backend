module.exports = (app) => {
	const places = require('../controllers/place.controller');

	// create a new Note
	app.post('/places', places.create);

	// retrieve all places
	app.get('/places', places.findAll);
	
	// retrieve one place
	app.get('/places/:placeId', places.findOne);

	// update a place with a note id
	app.put('/places/:placeId', places.update);

	// delete using a place
	app.delete('/places/:placeId', places.delete);
};
